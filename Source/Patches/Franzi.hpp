#ifndef __Franzi_hpp__
#define __Franzi_hpp__

//#include <stdlib.h>
#include "StompBox.h"
namespace KFranzi {
  class Buffer {
  private:
    float* buffer;
    unsigned int bufferSize;
    unsigned int writeIndex;
    unsigned int readIndex1;
    unsigned int readIndex2;
    float readMix; // 0 (just readIndex1) - 1 (readIndex2)
    float readMixInc;

    inline unsigned int inc(unsigned int index, int amount) const {
      unsigned int r = index + amount;
      return (r % bufferSize);
    }
  
  public:
    const static int MINSIZE = 400;
    const static int MAXSIZE = 88200;

    Buffer() {
      buffer = NULL;
      setBufferSize(MINSIZE);
    }

    inline void setBufferSize(unsigned int size) {
      if (bufferSize == size)
        return;

      bufferSize = size;
      if (buffer != NULL) free(buffer);
      buffer = (float *) calloc(bufferSize, sizeof(float));
      writeIndex = 0;
      readIndex1 = 1;
      readIndex2 = 1;
      readMix = 0;
    }

    inline void setWindowSize(unsigned int size) {
      readMixInc = 1.0f / size;
      if (readMixInc < 0.01) readMixInc = 0.01f;
    }

    inline void write(float value) {
      buffer[writeIndex] = value;
    }

    inline float readRecordHead() const {
      return buffer[writeIndex];
    }

    inline float readPlayHead() const {
      return buffer[readIndex1] * (1.0f - readMix) + buffer[readIndex2] * readMix;
    }
		
    inline void nextSample() {
      writeIndex = inc(writeIndex, 1);
      readIndex1 = inc(readIndex1, 1);
      readIndex2 = inc(readIndex2, 1);
      if (readMix < .999999) readMix += readMixInc;
    }

    inline unsigned int getWritePos() const {
      return writeIndex;
    }

    inline void jump(int size) {
      readIndex1 = readIndex2;
      readIndex2 = inc(readIndex2, size);
      readMix = 0;
    }
  };

  class Parameters {
	private:
    Patch* pPatch;

		inline unsigned int round(double x) const {
      return (unsigned int) (x + 0.5);	
		}
  public:
    Parameters(Patch* p) {
      pPatch = p;
      update();
			feedback = 1.0f;
    }
    inline void update() {
      double param_a = pPatch->getParameterValue(PARAMETER_A);
      bufferSize = round(param_a * 
                         (float)((Buffer::MAXSIZE - Buffer::MINSIZE)) + Buffer::MINSIZE);

      double param_b = pPatch->getParameterValue(PARAMETER_B);
			depth = (1.0f - param_b);

      double param_c = pPatch->getParameterValue(PARAMETER_C);
      density = round(22100.f * (1.0f - param_c)) + 1;

      double param_d = pPatch->getParameterValue(PARAMETER_D);
      length = round(22100.0f * param_d) + 10; 
    } 

    unsigned int bufferSize;
    unsigned int density;
    float depth;
    unsigned int length;
    float feedback;
  };

  class Cutter {
  private:
    enum State { INACTIVE, DEC, FULL, INC };

		Parameters* pParams;
    State state;
    int nextStateChange;
    float levelChange;
    float actualLevel;
    
  public:
    Cutter(Parameters* p) {
      pParams = p;
      state = INACTIVE;
      nextStateChange = rand() % pParams->density;
      actualLevel = 1.0f;
    }
    inline float calcCut() {
      switch (state) {
      case INACTIVE:
        actualLevel = 1.0f;
        break;
      case DEC:
      case INC:
        actualLevel += levelChange;
        break;
      }
      if (--nextStateChange <= 0) {
        switch(state) {
        case INACTIVE:
          state = DEC;
//          nextStateChange = rand() % (pParams->length / 4) + 20;
					nextStateChange = 100;
					levelChange = -pParams->depth / nextStateChange; 
          break;
        case DEC:
          state = FULL;
          nextStateChange = rand() % pParams->length;
          break;
        case FULL:
          state = INC;
          nextStateChange = rand() % (pParams->length / 4) + 20;
          levelChange = (1.f - actualLevel) / nextStateChange;
          break;
        case INC:
          state = INACTIVE;
          nextStateChange = rand() % pParams->density;
          break;
        }
      }
      return actualLevel;
    }
  };
} // end of namespace

using namespace KFranzi;


class Franzi : public Patch {
private:
public:
  Franzi() {
    pParams = new KFranzi::Parameters(this);
    pBuffer = new KFranzi::Buffer();
    pCutter = new Cutter(pParams);
  }
  KFranzi::Parameters* pParams;
  KFranzi::Buffer* pBuffer;
  Cutter* pCutter;

  void processAudio(AudioInputBuffer &input, AudioOutputBuffer &output) {
    int size = input.getSize();
    float* inBuf = input.getSamples();
    float* outBuf = output.getSamples();
		
    pParams->update();
    pBuffer->setBufferSize(pParams->bufferSize);

    for(int i = 0; i < size; ++i) {
      pBuffer->write((inBuf[i] + pParams->feedback * pBuffer->readRecordHead()) *
                     pCutter->calcCut());
      outBuf[i] = inBuf[i] + pBuffer->readPlayHead();
			pBuffer->nextSample();
    }
  }
};

#endif // __Franzi_hpp__
